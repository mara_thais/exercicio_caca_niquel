package itau.com.br;

import java.util.ArrayList;
import java.util.List;

public class Maquina {
    private List<Slot> slots;

    public Maquina(int dificuldade) {
        this.slots = new ArrayList<>();

        for (int i=0; i < dificuldade; i++){
            this.slots.add(new Slot());
        }
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public int calcularPontuacao(){
        int pontuacao = 0;

        for(Slot slot : this.slots){
            pontuacao = pontuacao + slot.getOpcao().pontos;
        }

        if(exiteBonus()){
            pontuacao = pontuacao*100;
        }

        return pontuacao;
    }

    public boolean exiteBonus(){
        boolean resposta = this.slots.stream()
                .distinct().limit(this.slots.size()).count() == 1;

        return resposta;
    }

    @Override
    public String toString() {
        return "Maquina{" +
                "slots=" + slots +
                '}';
    }
}
